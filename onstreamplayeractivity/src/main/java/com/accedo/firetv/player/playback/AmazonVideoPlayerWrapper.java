package com.accedo.firetv.player.playback;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.SurfaceView;
import android.view.WindowManager;

import com.accedo.firetv.player.OnStreamLogger;
import com.visualon.AmazonPlayReady.AmznPlayer;
import com.visualon.AmazonPlayReady.LicenseManager;
import com.visualon.OSMPPlayer.VOCommonPlayerListener;
import com.visualon.OSMPPlayer.VOOSMPInitParam;
import com.visualon.OSMPPlayer.VOOSMPOpenParam;
import com.visualon.OSMPPlayer.VOOSMPType;

import java.io.IOException;
import java.io.InputStream;

/**
 * Hides all the complexity from using a visual on video player
 */
public class AmazonVideoPlayerWrapper implements VideoPlayerWrapper, VOCommonPlayerListener {

    private static final long SEEK_DIVIDER = 10;

    private final Context context;
    private final LicenseManager licenseManager;

    private AmznPlayer amznPlayer;
    private VideoPlayerWrapperListener playerListener = VideoPlayerWrapperListener.NO_OP;

    //whether the player is currently paused or not
    private boolean playerPaused = false;

    public AmazonVideoPlayerWrapper(Context context,
                                    LicenseManager licenseManager) {
        this.context = context;
        this.licenseManager = licenseManager;
    }

    @Override
    public void initialize(String videoPath,
                           String licenseServer,
                           SurfaceView surfaceView,
                           VideoPlayerWrapperListener playerListener) {
        this.playerListener = playerListener;
        if (amznPlayer != null) {
            amznPlayer.resume(surfaceView);
            return;
        }

        // Initialize the SDK
        VOOSMPType.VO_OSMP_RETURN_CODE nRet;

        amznPlayer = new AmznPlayer();
        String apkPath = context.getApplicationInfo().dataDir + "/lib/";
        String cfgPath = context.getApplicationInfo().dataDir + "/";

        // SDK player engine type
        VOOSMPType.VO_OSMP_PLAYER_ENGINE eEngineType = VOOSMPType.VO_OSMP_PLAYER_ENGINE.VO_OSMP_VOME2_PLAYER;

        VOOSMPInitParam init = new VOOSMPInitParam();
        init.setContext(context);
        init.setLibraryPath(apkPath);

        // Initialize SDK player
        nRet = amznPlayer.init(eEngineType, init);

        // Set view
        amznPlayer.setView(surfaceView);

        //set drm values
        amznPlayer.setPlayReadyLicenseManager(licenseManager);
        amznPlayer.setPlayReadyLicenseServerURL(licenseServer);
        //we only care about playready for now, else we might need to change this
        amznPlayer.setDRMLibrary("voDRMMediaCrypto", "voGetDRMAPI");

        // Set surface view size
        DisplayMetrics dm = new DisplayMetrics();
        ((WindowManager)context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(dm);
        amznPlayer.setViewSize(dm.widthPixels, dm.heightPixels);

        // Register SDK event listener
        amznPlayer.setOnEventListener(this);

        if (nRet == VOOSMPType.VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE) {
            OnStreamLogger.v("MediaPlayer is created.");
        } else {
            playerListener.onError(nRet.getValue(), 0);
            return;
        }

        // Set device capability file location
        String capFile = cfgPath + "cap.xml";
        amznPlayer.setDeviceCapabilityByFile(capFile);
        // Set license file location
//        amznPlayer.setLicenseFilePath(cfgPath);

        InputStream is = null;
        byte[] b = new byte[32*1024];
        try {
            is = context.getAssets().open("voVidDec.dat");
            is.read(b);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        amznPlayer.setLicenseContent(b);
        amznPlayer.setPreAgreedLicense("VISUALON-AMAZON-2014AAC0FC094236");

        // Start playing the video
        // First open the media source
        // Auto-detect source format
        VOOSMPType.VO_OSMP_SRC_FORMAT format = VOOSMPType.VO_OSMP_SRC_FORMAT.VO_OSMP_SRC_AUTO_DETECT;

        // Set source flag to Asynchronous Open - Using Async Open
        VOOSMPType.VO_OSMP_SRC_FLAG eSourceFlag;
        eSourceFlag = VOOSMPType.VO_OSMP_SRC_FLAG.VO_OSMP_FLAG_SRC_OPEN_ASYNC;

        VOOSMPOpenParam openParam = new VOOSMPOpenParam();
        openParam.setDecoderType(VOOSMPType.VO_OSMP_DECODER_TYPE.VO_OSMP_DEC_VIDEO_SW.getValue() | VOOSMPType
                .VO_OSMP_DECODER_TYPE
                .VO_OSMP_DEC_AUDIO_SW.getValue());

        // Open media source
        nRet = amznPlayer.open(videoPath, eSourceFlag, format, openParam);

        if (nRet == VOOSMPType.VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE) {
            OnStreamLogger.v("MediaPlayer open Async returned ok");
        } else {
            playerListener.onError(nRet.getValue(), 0);
        }
    }

    @Override
    public void surfaceChanged() {
        if (amznPlayer != null) {
            amznPlayer.setSurfaceChangeFinished();
        }
    }

    @Override
    public void releaseMediaPlayer() {
        if (amznPlayer != null) {
            amznPlayer.stop();
            amznPlayer.close();
            amznPlayer.destroy();
            amznPlayer = null;
            playerListener = VideoPlayerWrapperListener.NO_OP;
            OnStreamLogger.v("MediaPlayer is released.");
        }
    }

    @Override
    public boolean playerPauseRestart() {
        if (amznPlayer != null) {
            if (!playerPaused) {
                if (amznPlayer.canBePaused()) {
                    amznPlayer.pause();
                    playerPaused = true;
                }
            } else {
                amznPlayer.start();
                playerPaused = false;
            }
        }
        return playerPaused;
    }

    private void seek(long delta) {
        long duration = amznPlayer.getDuration();
        long newPosition = amznPlayer.getPosition() + delta;
        if (newPosition > duration) {
            newPosition = duration;
        }
        OnStreamLogger.v("Seek To " + newPosition);
        amznPlayer.setPosition(newPosition); // Set new position
    }

    @Override
    public void seekForward() {
        if (amznPlayer != null) {
            seek(amznPlayer.getDuration()/SEEK_DIVIDER);
        }
    }

    @Override
    public void seekBackward() {
        if (amznPlayer != null) {
            seek(-amznPlayer.getDuration()/SEEK_DIVIDER);
        }
    }

    @Override
    public void seekTo(float durationPercentage) {
        if (amznPlayer != null) {
            amznPlayer.setPosition((long) (amznPlayer.getDuration()*durationPercentage));
        }
    }

    @Override
    public long[] getPositionInfo() {
       long[] result = new long[4];
        if(amznPlayer != null) {
            result[0] = amznPlayer.getDuration();
            result[1] = amznPlayer.getPosition();
            result[2] = amznPlayer.getMinPosition();
            result[3] = amznPlayer.getMaxPosition();
        }
        return result;
    }

    @Override
    public VOOSMPType.VO_OSMP_RETURN_CODE onVOEvent(VO_OSMP_CB_EVENT_ID eventId, int nParam1, int nParam2, Object obj) {
        switch (eventId) {
            case VO_OSMP_CB_ERROR:
            case VO_OSMP_SRC_CB_CONNECTION_FAIL:
            case VO_OSMP_SRC_CB_DOWNLOAD_FAIL:
            case VO_OSMP_SRC_CB_DRM_FAIL:
            case VO_OSMP_SRC_CB_PLAYLIST_PARSE_ERR:
            case VO_OSMP_SRC_CB_CONNECTION_REJECTED:
            case VO_OSMP_SRC_CB_DRM_NOT_SECURE:
            case VO_OSMP_SRC_CB_DRM_AV_OUT_FAIL:
            case VO_OSMP_CB_LICENSE_FAIL:
                playerListener.onError(eventId.getValue(), 0);
                break;
            case VO_OSMP_SRC_CB_OPEN_FINISHED:
                onAsyncOpenFinished(nParam1);
                break;
            case VO_OSMP_CB_PLAY_COMPLETE:
                playerListener.onPlaybackComplete();
                break;
            case VO_OSMP_CB_VIDEO_SIZE_CHANGED:
                playerListener.onVideoSizeChanged(nParam1, nParam2);
                break;
            case VO_OSMP_CB_VIDEO_START_BUFFER:
                playerListener.onBufferStart();
                break;
            case VO_OSMP_CB_VIDEO_STOP_BUFFER:
                playerListener.onBufferStop();
                break;
        }
        return VOOSMPType.VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE;
    }

    @Override
    public VOOSMPType.VO_OSMP_RETURN_CODE onVOSyncEvent(VO_OSMP_CB_SYNC_EVENT_ID eventId,
                                                        int arg1,
                                                        int arg2,
                                                        Object arg3) {
        return VOOSMPType.VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE;
    }

    private void onAsyncOpenFinished(int errorCode) {
        OnStreamLogger.v("Async Open Finished...");
        if (errorCode == VOOSMPType.VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE.getValue()) {
            OnStreamLogger.v("MediaPlayer is opened (async open).");
            VOOSMPType.VO_OSMP_RETURN_CODE returnCode;

            // Start (play) media pipeline
            returnCode = amznPlayer.start();
            if (returnCode == VOOSMPType.VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE) {
                OnStreamLogger.v("MediaPlayer run.");
                playerListener.onOpenSuccess();
            } else {
                playerListener.onError(returnCode.getValue(), 0);
            }
        } else {
            playerListener.onError(errorCode, 0);
        }
    }
}
