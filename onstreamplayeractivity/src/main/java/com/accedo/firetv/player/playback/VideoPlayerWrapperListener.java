package com.accedo.firetv.player.playback;

/**
 * Listens for Video Player issues
 */
public interface VideoPlayerWrapperListener {
    VideoPlayerWrapperListener NO_OP = new VideoPlayerWrapperListener() {
        @Override
        public void onError(int what, int extra) {

        }

        @Override
        public void onOpenSuccess() {

        }

        @Override
        public void onVideoSizeChanged(int width, int height) {

        }

        @Override
        public void onPlaybackComplete() {

        }

        @Override
        public void onBufferStart() {

        }

        @Override
        public void onBufferStop() {

        }
    };

    void onError(int what, int extra);

    void onOpenSuccess();

    void onVideoSizeChanged(int width, int height);

    void onPlaybackComplete();

    void onBufferStart();

    void onBufferStop();
}
