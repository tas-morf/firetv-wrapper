package com.accedo.firetv.player.module;

import android.content.Context;

import com.accedo.firetv.player.OnStreamPlayerApplication;


public class ApplicationModule {

	private static OnStreamPlayerApplication exampleApplication;

	public static void setApplication(OnStreamPlayerApplication exampleApplication) {
		ApplicationModule.exampleApplication = exampleApplication;
	}

	public static Context applicationContext(){
		return exampleApplication;
	}
}
