package com.accedo.firetv.player;


import android.app.Application;

import com.accedo.firetv.player.module.ApplicationModule;
import com.accedo.firetv.player.playback.VisualOnFileCopier;


public class OnStreamPlayerApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
        ApplicationModule.setApplication(this);
        VisualOnFileCopier.copyVisualOnFiles(this);
	}
}
