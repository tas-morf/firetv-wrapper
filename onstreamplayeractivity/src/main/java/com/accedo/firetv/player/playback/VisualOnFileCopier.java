package com.accedo.firetv.player.playback;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Handles the copying of visual on specific files
 */
public class VisualOnFileCopier {

    public static void copyVisualOnFiles(Context context) {
        copyfile(context, "voVidDec.dat", "voVidDec.dat");
        copyfile(context, "cap.xml", "cap.xml");
    }

    // Copy file from Assets directory to destination
    // Used for licenses and processor-specific configurations
    private static void copyfile(Context context, String filename, String desName) {
        try {
            InputStream is = context.getAssets().open(filename);
            File desFile = new File(context.getApplicationInfo().dataDir + "/" + desName);
            if(desFile.createNewFile()) {
                FileOutputStream fos = new FileOutputStream(desFile);
                int bytesRead;
                byte[] buf = new byte[4 * 1024]; // 4K buffer

                while ((bytesRead = is.read(buf)) != -1) {
                    fos.write(buf, 0, bytesRead);
                }
                fos.flush();
                fos.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
