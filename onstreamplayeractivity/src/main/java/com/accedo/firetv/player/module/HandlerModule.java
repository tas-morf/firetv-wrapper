package com.accedo.firetv.player.module;


import android.os.Handler;

import static com.accedo.firetv.player.module.ApplicationModule.applicationContext;


public class HandlerModule {
    public static Handler mainThreadHandler() {
        return new Handler(applicationContext().getMainLooper());
    }
}
