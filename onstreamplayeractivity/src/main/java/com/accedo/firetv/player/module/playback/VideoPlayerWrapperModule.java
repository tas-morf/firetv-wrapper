package com.accedo.firetv.player.module.playback;


import com.accedo.firetv.player.playback.AmazonVideoPlayerWrapper;
import com.accedo.firetv.player.playback.VideoPlayerWrapper;
import com.visualon.AmazonPlayReady.LicenseManager;

import static com.accedo.firetv.player.module.ApplicationModule.applicationContext;

public class VideoPlayerWrapperModule {
    public static VideoPlayerWrapper videoPlayerWrapper() {

        return new AmazonVideoPlayerWrapper(applicationContext(), new LicenseManager(applicationContext()));
    }
}
