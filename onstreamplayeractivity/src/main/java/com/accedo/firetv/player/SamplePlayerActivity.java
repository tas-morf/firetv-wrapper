/************************************************************************
VisualOn Proprietary
Copyright (c) 2013, VisualOn Incorporated. All rights Reserved

VisualOn, Inc., 4675 Stevens Creek Blvd, Santa Clara, CA 95051, USA

All data and information contained in or disclosed by this document are
confidential and proprietary information of VisualOn, and all rights
therein are expressly reserved. By accepting this material, the
recipient agrees that this material and the information contained
therein are held in confidence and in trust. The material may only be
used and/or disclosed as authorized in a license agreement controlling
such use and disclosure.
************************************************************************/

package com.accedo.firetv.player;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.visualon.AmazonPlayReady.AmznPlayer;
import com.visualon.AmazonPlayReady.LicenseManager;
import com.visualon.OSMPPlayer.VOCommonPlayer;
import com.visualon.OSMPPlayer.VOCommonPlayerAssetSelection;
import com.visualon.OSMPPlayer.VOCommonPlayerAssetSelection.VOOSMPAssetIndex;
import com.visualon.OSMPPlayer.VOCommonPlayerAssetSelection.VOOSMPAssetProperty;
import com.visualon.OSMPPlayer.VOCommonPlayerListener;
import com.visualon.OSMPPlayer.VOOSMPInitParam;
import com.visualon.OSMPPlayer.VOOSMPOpenParam;
import com.visualon.OSMPPlayer.VOOSMPType.VO_OSMP_ASPECT_RATIO;
import com.visualon.OSMPPlayer.VOOSMPType.VO_OSMP_DECODER_TYPE;
import com.visualon.OSMPPlayer.VOOSMPType.VO_OSMP_DOWNLOAD_STATUS;
import com.visualon.OSMPPlayer.VOOSMPType.VO_OSMP_PLAYER_ENGINE;
import com.visualon.OSMPPlayer.VOOSMPType.VO_OSMP_RETURN_CODE;
import com.visualon.OSMPPlayer.VOOSMPType.VO_OSMP_SOURCE_STREAMTYPE;
import com.visualon.OSMPPlayer.VOOSMPType.VO_OSMP_SRC_FLAG;
import com.visualon.OSMPPlayer.VOOSMPType.VO_OSMP_SRC_FORMAT;
import com.visualon.OSMPUtils.voLog;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class SamplePlayerActivity extends Activity
{
    private static final String  TAG                  = "@@@OSMP+Player"; // Tag for VOLog messages

    /* Messages for managing the user interface */
    private static final int     MSG_SHOW_CONTROLLER  = 1;
    private static final int     MSG_HIDE_CONTROLLER  = 2;
    private static final int     MSG_UPDATE_UI        = 3;
    private static final int     MSG_PLAYCOMPLETE     = 5;
    private static final int     MSG_STARTPLAY        = 10;
    
    private static final String  STRING_ASSETPROPERTYNAME_VIDEO         = "V";
    private static final String  STRING_ASSETPROPERTYNAME_AUDIO         = "A";
    private static final String  STRING_ASSETPROPERTYNAME_SUBTITLE      = "Subt";

    /* SurfaceView must be passed to SDK */
    private SurfaceView          m_svMain             = null;
    private SurfaceHolder        m_shMain             = null;

    /* Media controls and User interface */
    private ImageButton          m_ibPlayPause        = null;            // Play/Pause button
    private SeekBar              m_sbMain             = null;            // Seekbar

    private TextView             m_tvCurrentTime      = null;            // Current position
    private TextView             m_tvTotalTime        = null;            // Total duration

    private ProgressBar          m_pbLoadingProgress  = null;            // Wait icon for buffered or stopped video
    private Date                 m_dateUIDisplayStartTime = null;        // Last update of media controls

    private Timer                m_timerMain          = null;            // Timer for display of media controls
    private TimerTask            m_ttMain             = null;            // Timer Task for display of media controls

    private AmznPlayer          m_sdkPlayer          = null;            // Amzn player
    private String	        	m_LA_URL             = null;            // custom LA_URL

    /* Flags */
    private boolean              m_bTrackProgressing  = false;           // Seekbar flag

    private boolean              m_bPaused            = false;           // Pause flag
    private boolean              m_bStoped            = false;           // Stop flag

    private String               m_strVideoPath       = null;              // URL or file path to media source
    private String               m_strSubtitlePath    = "";

    private int                  m_nVideoWidth        = 0;               // Video width
    private int                  m_nVideoHeight       = 0;               // video height
    private int                  m_nDuration          = 0;               // Total duration
    private int                  m_nPos               = 0;               // Current position

    private int                  m_nBitRate           = -1;               // current bitrate

    /* User interface for main view (media source input/selection) */
    private EditText             m_edtInputURL        = null;            // User input URL or file path
    private AlertDialog          m_adlgMain           = null;            // Dialog for media source
    private Spinner              m_spSelectURL        = null;            // Media source list selector
    private ArrayList<String>    m_lstSelectURL       = null;            // Media source list from url.txt
    private Spinner              m_spSelectSubtitle   = null;            // External subtitle list selector
    private ArrayList<String>    m_lstSelectSubtitle  = null;            // External subtitle String list
    private Spinner              m_spSelectCustomLAURL  = null;          // Custom LA_URL list selector
    private ArrayList<String>    m_lstSelectCustomLAURL = null;          // Custom LA_URL list
    private boolean              m_bSpinnerClickAutoTriggered_SelectUrl = false;
    private boolean              m_bSpinnerClickAutoTriggered_SelectSubtitle = false;
    private boolean              m_bSpinnerClickAutoTriggered_SelectCustomUrl = false;
    
    private CheckBox             m_chbAsync           = null;
    
    private boolean              m_bEnableVideo       = true; // BpsQuality 
    private boolean              m_bEnableAudio       = true; // AudioTrack
    private boolean              m_bEnableSubtitle    = true; // External/Internal Subtitle or Closed Caption
    private boolean              m_isResume                = false;
    private boolean              m_bEnableChannel     = true;
    private boolean              m_bShowOpenMAXAL     = false;

    private TableLayout          m_rlTop              = null;
    private RelativeLayout       m_rlBottom           = null;
    private RelativeLayout       m_rlRight            = null;      // reserved
    private HorizontalScrollView m_hsvRight           = null;
    private LinearLayout         m_llRight            = null;
    private LinearLayout         m_llSubtitlePopupMenu= null;
    private LinearLayout         m_llVideoPopupMenu   = null;
    private LinearLayout         m_llAudioPopupMenu   = null;
    private HorizontalScrollView m_hsvSubtitlePopupMenu = null;
    private HorizontalScrollView m_hsvVideoPopupMenu  = null; 
    private HorizontalScrollView m_hsvAudioPopupMenu  = null;

    private RelativeLayout       m_rlProgramInfo      = null;
    private RelativeLayout       m_rlProgramInfoArrow = null;
    
    private TextView             m_statusView         = null;
    private TextView             m_tvCommit           = null;
    private TextView             m_tvRevert           = null;

    private RelativeLayout       m_rlChannel          = null;
    private ListView             m_lvChannel          = null;
    
    private int                  m_nPlayingVideoIndex     = VOCommonPlayerAssetSelection.VO_OSMP_ASSET_AUTO_SELECTED;  // index of playing video, -1 is Auto. 
    private int                  m_nPlayingAudioIndex     = 0;
    private int                  m_nPlayingSubtitleIndex  = 0;
    
    private int                  m_nSelectedVideoIndex    = 0;  // index of selected video, 0 is Auto 
    private int                  m_nSelectedAudioIndex    = 0;
    private int                  m_nSelectedSubtitleIndex = 0;
    private BroadcastReceiver    m_InfoReceiver           = null;
    private enum AssetType {
        Asset_Video,          // BpsQuality 
        Asset_Audio,          // AudioTrack 
        Asset_Subtitle        // External/Internal Subtitle or CloseCaption
    }
    
    private SurfaceHolder.Callback m_cbSurfaceHolder = new SurfaceHolder.Callback() {
        /* Notify SDK on Surface Change */  
        public void surfaceChanged (SurfaceHolder surfaceholder, int format, int w, int h) {
            voLog.i(TAG, "Surface Changed");
            if (m_sdkPlayer != null)
                m_sdkPlayer.setSurfaceChangeFinished();
        }

        /* Notify SDK on Surface Creation */  
        public void surfaceCreated(SurfaceHolder surfaceholder) {
            voLog.i(TAG, "Surface Created");
            if (m_sdkPlayer !=null&& m_isResume) {
                // For handling the situation such as phone calling is coming.
                
                // If SDK player already exists, show media controls
                m_sdkPlayer.resume(m_svMain);
                m_isResume=false;
                showMediaController();
                return;
            }
            if(m_sdkPlayer !=null)
                return;
            if ((m_strVideoPath == null) || (m_strVideoPath.trim().length() <=0))
                return;
            
            // Enter from the other APP such as a browser or file explorer
            initPlayer(m_strVideoPath);
            playVideo(m_strVideoPath);
        }
      
        public void surfaceDestroyed(SurfaceHolder surfaceholder) {
            voLog.i(TAG, "Surface Destroyed");
            if (m_sdkPlayer != null)
                m_sdkPlayer.setView(null);
        }
    };
    
    private VOCommonPlayerListener m_listenerEvent = new VOCommonPlayerListener() {
        /* SDK event handling */
     
        @SuppressWarnings("incomplete-switch")
        public VO_OSMP_RETURN_CODE onVOEvent(VO_OSMP_CB_EVENT_ID nID, int nParam1, int nParam2, Object obj) {
            switch(nID) { 
            case VO_OSMP_SRC_CB_ADAPTIVE_STREAMING_INFO: {
                voLog.i(TAG, "onVOEvent VO_OSMP_SRC_CB_ADAPTIVE_STREAMING_INFO " + nParam1);
                VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT event = VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT.valueOf(nParam1);
                switch(event) {
                    case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_BITRATE_CHANGE: {
                        voLog.v(TAG, "OnEvent VOOSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_BITRATE_CHANGE " + nParam2);
                        m_nBitRate = nParam2;
                        updateStatusViewText();
                        break;
                    }
                    case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_CHUNK_BEGINDOWNLOAD :
                        voLog.v(TAG, "OnEvent VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_CHUNK_BEGINDOWNLOAD");
                        break;
                    case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_CHUNK_DOWNLOADOK :
                        voLog.v(TAG, "OnEvent VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_CHUNK_DOWNLOADOK");
                        break;
                    case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_CHUNK_DROPPED :
                        voLog.v(TAG, "OnEvent VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_CHUNK_DROPPED");
                        break;
                    case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_FILE_FORMATSUPPORTED:
                        voLog.v(TAG, "OnEvent VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_FILE_FORMATSUPPORTED");
                        break;
                    case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_LIVESEEKABLE:
                        voLog.v(TAG, "OnEvent VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_LIVESEEKABLE");
                        break;
                    case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_MEDIATYPE_CHANGE: {
                        voLog.v(TAG, "OnEvent VOOSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_MEDIATYPE_CHANGE");
                        VO_OSMP_AVAILABLE_TRACK_TYPE type = VO_OSMP_AVAILABLE_TRACK_TYPE.valueOf(nParam2);
                        switch(type) {
                            case VO_OSMP_AVAILABLE_PUREAUDIO: {
                                voLog.v(TAG, "OnEvent VOOSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_MEDIATYPE_CHANGE, VOOSMP_AVAILABLE_PUREAUDIO");
                                break;
                            }
                            case VO_OSMP_AVAILABLE_PUREVIDEO: {
                                voLog.v(TAG, "OnEvent VOOSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_MEDIATYPE_CHANGE, VOOSMP_AVAILABLE_PUREVIDEO");
                                break;
                            }
                            case VO_OSMP_AVAILABLE_AUDIOVIDEO: {
                                voLog.v(TAG, "OnEvent VOOSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_MEDIATYPE_CHANGE, VOOSMP_AVAILABLE_AUDIOVIDEO");
                                break;
                            }
                        }
                        break;
                    }
                    case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_PLAYLIST_DOWNLOADOK:
                        voLog.v(TAG, "OnEvent VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_PLAYLIST_DOWNLOADOK");
                        break;
                    case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_PROGRAM_CHANGE:
                        voLog.v(TAG, "OnEvent VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_PROGRAM_CHANGE");
                        break;
                    case VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_PROGRAM_TYPE:
                        voLog.v(TAG, "OnEvent VO_OSMP_SRC_ADAPTIVE_STREAMING_INFO_EVENT_PROGRAM_TYPE");
                        break;
                }
                break;
            }
            case VO_OSMP_SRC_CB_ADAPTIVE_STREAMING_ERROR: {
                VO_OSMP_SRC_ADAPTIVE_STREAMING_ERROR_EVENT
                event = VO_OSMP_SRC_ADAPTIVE_STREAMING_ERROR_EVENT.valueOf(nParam1);
                voLog.v(TAG, "Recevied VOOSMP_SRC_ADAPTIVE_STREAMING_ERROR_EVENT " + nParam1);
                if (event == VO_OSMP_SRC_ADAPTIVE_STREAMING_ERROR_EVENT.VO_OSMP_SRC_ADAPTIVE_STREAMING_ERROR_EVENT_STREAMING_DRMLICENSEERROR) {
                    voLog.v(TAG, "Encounter DRM errors, stop playback");
                    // DRM error, stop palyback
                    handler.sendEmptyMessage(MSG_PLAYCOMPLETE);
                }
                break;
            }
            case VO_OSMP_CB_LICENSE_FAIL: { 
                voLog.v(TAG, "OSMP license checking failed");
                break;
            }
            case VO_OSMP_CB_ERROR:  
            case VO_OSMP_SRC_CB_CONNECTION_FAIL:
            case VO_OSMP_SRC_CB_DOWNLOAD_FAIL:
            case VO_OSMP_SRC_CB_DRM_FAIL:
            case VO_OSMP_SRC_CB_PLAYLIST_PARSE_ERR:
            case VO_OSMP_SRC_CB_CONNECTION_REJECTED:
            case VO_OSMP_SRC_CB_DRM_NOT_SECURE:
            case VO_OSMP_SRC_CB_DRM_AV_OUT_FAIL: {  // Error
                // Display error dialog and stop player
                onError(m_sdkPlayer, getString(R.string.str_ErrPlay_Message), nID.getValue(), 0);
                break;
            }
            case VO_OSMP_CB_PLAY_COMPLETE: {
                handler.sendEmptyMessage(MSG_PLAYCOMPLETE);
                break;
            }   
            case VO_OSMP_CB_SEEK_COMPLETE: {        // Seek (SetPos) complete
                voLog.v(TAG, "Receive Engine Seek Complete");
                break;
            }
            case VO_OSMP_SRC_CB_SEEK_COMPLETE : {   // Seek (SetPos) complete
                voLog.v(TAG, "Receive Source Seek Complete");
                break;
            }
            case VO_OSMP_CB_VIDEO_RENDER_START : { 
                voLog.v(TAG, "Receive VideoRenderStart");
                break;
            }
            case VO_OSMP_CB_VIDEO_SIZE_CHANGED: {   // Video size changed
                m_nVideoWidth = nParam1;
                m_nVideoHeight = nParam2;

                updateStatusViewText();
                
                // Retrieve new display metrics
                DisplayMetrics dm  = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(dm);
                
                if (getResources().getConfiguration().orientation
                        == Configuration.ORIENTATION_PORTRAIT) {
                    
                    // If portrait orientation, scale height as a ratio of the new aspect ratio
                    ViewGroup.LayoutParams lp = m_svMain.getLayoutParams();
                    lp.width = dm.widthPixels;
                    lp.height = dm.widthPixels * m_nVideoHeight / m_nVideoWidth;
                    m_svMain.setLayoutParams(lp);
                }
                
                break;
            }   
            case VO_OSMP_CB_VIDEO_ASPECT_RATIO: {
                voLog.v(TAG, "OnEvent  VO_OSMP_CB_VIDEO_ASPECT_RATIO, param1 is %d", nParam1, nParam2);
                break;
            }
            case VO_OSMP_CB_VIDEO_STOP_BUFFER: {    // Video buffering stopped
                m_pbLoadingProgress.setVisibility(View.GONE);                   // Hide wait icon
                break;
            }
            case VO_OSMP_CB_VIDEO_START_BUFFER: {   // Video buffering started
                m_pbLoadingProgress.setVisibility(View.VISIBLE);                // Show wait icon   
                break;
            }
            case VO_OSMP_CB_AUDIO_STOP_BUFFER: {    // Video buffering stopped
                m_pbLoadingProgress.setVisibility(View.GONE);                   // Hide wait icon
                break;
            }
            case VO_OSMP_CB_AUDIO_START_BUFFER: {   // Video buffering started
                m_pbLoadingProgress.setVisibility(View.VISIBLE);                // Show wait icon   
                break;
            }
            case VO_OSMP_SRC_CB_BA_HAPPENED: {      // Unimplemented
                voLog.v(TAG, "OnEvent VOOSMP_SRC_CB_BA_Happened, param is %d " + nParam1);
                break;
            }
            case VO_OSMP_SRC_CB_DOWNLOAD_FAIL_WAITING_RECOVER: {
                voLog.v(TAG, "OnEvent VOOSMP_SRC_CB_Download_Fail_Waiting_Recover, param is %d " + nParam1);
                break;
            }
            case VO_OSMP_SRC_CB_DOWNLOAD_FAIL_RECOVER_SUCCESS: {
                voLog.v(TAG, "OnEvent VOOSMP_SRC_CB_Download_Fail_Recover_Success, param is %d " + nParam1);
                break;
            }
            case VO_OSMP_SRC_CB_OPEN_FINISHED: {
                voLog.v(TAG, "OnEvent VOOSMP_SRC_CB_Open_Finished, param is %x " + nParam1);
                
                if (nParam1 == VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE.getValue())  {
                    voLog.v(TAG, "MediaPlayer is opened async.");
                        
                    VO_OSMP_RETURN_CODE nRet;
                        
                    /* Run (play) media pipeline */
                    nRet = m_sdkPlayer.start();
                 
                    if (nRet == VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE) {
                        voLog.v(TAG, "MediaPlayer run async");
                    } else {
                        onError(m_sdkPlayer, getString(R.string.str_ErrPlay_Message), nRet.getValue(), 0);
                    }
                        
                    m_nPos = (int) m_sdkPlayer.getPosition();
                    m_nDuration = (int) m_sdkPlayer.getDuration();
                            
                    updatePosDur();
                       
                    m_bStoped = false;
                    m_ibPlayPause.setImageResource(R.drawable.selector_btn_pause);
                        
                }else {
                    onError(m_sdkPlayer, getString(R.string.str_ErrOpenFail), nParam1, 0);
                }
                    
                break;
            }
        }
    
        return VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE;       
        }

        @Override
        public VO_OSMP_RETURN_CODE onVOSyncEvent(VO_OSMP_CB_SYNC_EVENT_ID arg0,
                int arg1, int arg2, Object arg3) {
            // TODO Auto-generated method stub
            return VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE;
        }
    };
    
    private OnSeekBarChangeListener m_listenerSeekBar = new OnSeekBarChangeListener() {
        
        /* Seek to new position when Seekbar drag is complete */
        public void onStopTrackingTouch(SeekBar arg0) {
            m_bTrackProgressing = false;           // Disable Seekbar tracking flag
            
            // Calculate new position as percentage of total duration
            int nCurrent = arg0.getProgress();
            int nMax = arg0.getMax();
            
            long lNewPosition;
            if (m_nDuration > 0) {
                lNewPosition = nCurrent * m_nDuration / nMax;
            }
            else {
                long nMinPos = m_sdkPlayer.getMinPosition();
                long nMaxPos = m_sdkPlayer.getMaxPosition();
                int nDuration = (int) (nMaxPos - nMinPos);
                lNewPosition = (long)nCurrent * nDuration / nMax;
                lNewPosition = lNewPosition + nMinPos;
            }

            if (m_sdkPlayer != null) {
                voLog.v(TAG,"Seek To " + lNewPosition);
                m_sdkPlayer.setPosition(lNewPosition);  // Set new position
            }

        }
        
        /* Flag when Seekbar is being dragged */
        public void onStartTrackingTouch(SeekBar arg0) {
            m_bTrackProgressing = true;
        }
        
        public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {}
    };

    private Handler handler = new Handler() {
        /* Handler to manage user interface during playback */
        public void handleMessage(Message msg) {
            if (msg.what == MSG_STARTPLAY) {
                initPlayer(m_strVideoPath);
                playVideo(m_strVideoPath);
                return;
            }

            if(m_sdkPlayer == null)
                return;

            if (msg.what == MSG_SHOW_CONTROLLER) {
                /* Show media controls */
                showMediaControllerImpl();
            } else if (msg.what == MSG_HIDE_CONTROLLER) {
                /* Hide media controls */
                hideControllerImpl();
            } else if (msg.what == MSG_UPDATE_UI) {
                // Update UI
                doUpdateUI();

            } else if (msg.what == MSG_PLAYCOMPLETE) {
                /* Playback in complete, stop player */
                stopAndFinish();
            }
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        voLog.v(TAG, "Player onCreate");

        /*Screen always on*/
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, 
                             WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.player);

        // init GUI layout 
        initUI();    

        m_strVideoPath = getIntent().getStringExtra("URL");
        voLog.v(TAG, "onCreate m_strVideoPath " + m_strVideoPath);
        m_LA_URL = getIntent().getStringExtra("LA_URL");

        getWindow().setFormat(PixelFormat.UNKNOWN);

        // Find View and UI objects
        m_svMain = (SurfaceView) findViewById(R.id.svMain);
        m_shMain = m_svMain.getHolder();
        m_shMain.addCallback(m_cbSurfaceHolder);
        m_shMain.setFormat(PixelFormat.RGBA_8888);

        m_statusView = new TextView(this);
        m_statusView.setBackgroundColor(R.drawable.translucent_background);
        m_statusView.setTextColor(getResources().getColor(R.color.solid_red));
        RelativeLayout rl = (RelativeLayout)(m_svMain.getParent());
        rl.addView(m_statusView);

        m_ibPlayPause = (ImageButton) findViewById(R.id.ibPlayPause); 

        m_sbMain = (SeekBar) findViewById(R.id.sbMain);

        m_tvCurrentTime = (TextView)findViewById(R.id.tvCurrentTime);
        m_tvTotalTime = (TextView)findViewById(R.id.tvTotalTime); 
        m_pbLoadingProgress = (ProgressBar) findViewById(R.id.pbBuffer);

        m_sbMain.setOnSeekBarChangeListener(m_listenerSeekBar);

        voLog.v(TAG, "Video source is " + m_strVideoPath);

        m_rlTop.setVisibility(View.INVISIBLE);
        m_rlBottom.setVisibility(View.INVISIBLE);
        m_pbLoadingProgress.setVisibility(View.GONE);

        // Activate listener for Play/Pause button
        m_ibPlayPause.setOnClickListener(new ImageButton.OnClickListener() {
            public void onClick(View view) {
                playerPauseRestart();
            }
        });

        CommonFunc.copyfile(this, "cap.xml", "cap.xml");
        registerLockReceiver();

        if (m_strVideoPath == null)
            SourceWindow();
    }
    private void registerLockReceiver() {
        final IntentFilter filter = new IntentFilter(); 
        filter.addAction(Intent.ACTION_USER_PRESENT);   
            
      m_InfoReceiver = new BroadcastReceiver() {    
            @Override    
            public void onReceive(final Context context, final Intent intent) {  
                  
  
                String action = intent.getAction();    
  
                  
               if(Intent.ACTION_USER_PRESENT.equals(action))  
               {  
                   if(m_sdkPlayer != null && m_isResume)
                   {
                       m_sdkPlayer.resume(m_svMain);
                       m_isResume = false;
                   }
               }  
                 
            }    
        };    
          
        registerReceiver(m_InfoReceiver, filter);  
    }

    @Override
    protected void onStart() {
        voLog.v(TAG, "Player onStart");
        super.onStart();
    }

    /* Pause/Stop playback on activity pause */
    protected void onPause() {
        voLog.v(TAG, "Player onPause");
        super.onPause();
        if (m_sdkPlayer!= null) {
            m_isResume = true;
            m_sdkPlayer.suspend(false);
          }
    }
    @Override
    protected void onResume() {
        
       if (m_sdkPlayer !=null)
        {
            m_isResume = true;
        }
        super.onResume();
    }


    @Override
    protected void onRestart() {
        voLog.v(TAG, "Player onRestart");
        super.onRestart();
        if(m_sdkPlayer == null)
            return;
    }
	
    @Override
    protected void onStop() {
        voLog.v(TAG, "Player onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        voLog.v(TAG, "Player onDestroy");
        unregisterReceiver(m_InfoReceiver);
        stopVideo();
        uninitPlayer();
        super.onDestroy();  
        voLog.v(TAG, "Player onDestroy Completed!");
    }

    /* Notify SDK of configuration change */  
    public void onConfigurationChanged(Configuration newConfig) {

        if (m_sdkPlayer == null ||  m_nVideoHeight == 0 || m_nVideoWidth == 0) {     
            super.onConfigurationChanged(newConfig);
            return;
        }

        // Retrieve new display metrics
        DisplayMetrics dm  = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        ViewGroup.LayoutParams lp = m_svMain.getLayoutParams();
        lp.width = dm.widthPixels;

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // If landscape orientation, use display metrics height
            lp.height = dm.heightPixels;
            m_sdkPlayer.setVideoAspectRatio(VO_OSMP_ASPECT_RATIO.VO_OSMP_RATIO_00);

        }else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            // If portrait orientation, scale height as a ratio of the original aspect ratio
            lp.height = dm.widthPixels * m_nVideoHeight / m_nVideoWidth;
        }
        // Pass new width/height to View
        m_svMain.setLayoutParams(lp);

        super.onConfigurationChanged(newConfig);
    }
    
    private void setupParameters() {
        // Set view
        m_sdkPlayer.setView(m_svMain);

        DisplayMetrics dm  = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        // Set view size
        m_sdkPlayer.setViewSize(dm.widthPixels, dm.heightPixels);

        // Register SDK event listener
        m_sdkPlayer.setOnEventListener(m_listenerEvent);

        // Processor-specific settings 
        String capFile = CommonFunc.getUserPath(this) + "/" + "cap.xml";
        m_sdkPlayer.setDeviceCapabilityByFile(capFile);

        // Setup license 
        InputStream is = null;
        byte[] b = new byte[32*1024];
        try {
            is = getAssets().open("voVidDec.dat");
            is.read(b);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        m_sdkPlayer.setLicenseContent(b);

        // Set Pre-agreed license string if needed. This is required
        // depending on the license file. 
        m_sdkPlayer.setPreAgreedLicense("VISUALON-AMAZON-2014AAC0FC094236");

        if (m_strSubtitlePath != null && m_strSubtitlePath.length() > 0)
            m_sdkPlayer.setSubtitlePath(m_strSubtitlePath);

        // enable subtitle/closed caption
        enableSubtitleOutput(true);
    }

    /* Initialize SDK player and playback selected media source */
    private void initPlayer(String strPath) { 
        VO_OSMP_RETURN_CODE nRet;

        //m_sdkPlayer = new VOCommonPlayerImpl();
        m_sdkPlayer = new AmznPlayer();

        m_shMain.setType(SurfaceHolder.SURFACE_TYPE_NORMAL);

        // Location of libraries
        String apkPath = CommonFunc.getUserPath(this) + "/lib/";	

        // SDK player engine type
        VO_OSMP_PLAYER_ENGINE eEngineType = VO_OSMP_PLAYER_ENGINE.VO_OSMP_VOME2_PLAYER;

        // Initialize SDK player
        VOOSMPInitParam initParam = new VOOSMPInitParam();
        initParam.setContext(this);
        initParam.setLibraryPath(apkPath);
        nRet = m_sdkPlayer.init(eEngineType, initParam);

        if (nRet == VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE) {
            voLog.v(TAG, "MediaPlayer is initialized.");
        } else {
            onError(m_sdkPlayer, getString(R.string.str_ErrInitFail), nRet.getValue(), 0);
            return;
        }

        setupParameters();

        // If using PlayReady DRM, set PlayReady license manager
        m_sdkPlayer.setPlayReadyLicenseManager(new LicenseManager(this));
    }

    private void playVideo(String strPath) { 
        VO_OSMP_RETURN_CODE nRet;

        voLog.v(TAG, "LA_URL is " + m_LA_URL);
        m_sdkPlayer.setPlayReadyLicenseServerURL(m_LA_URL);

        /* Set DRM library */
        // assume PlayReady for DASH and SmoothStreaming, AES128 for HLS
        if(strPath==null){
            Toast.makeText(getApplicationContext(), "Input video path is null!", Toast.LENGTH_LONG).show();
            return;
        }
        if (strPath.endsWith(".m3u8") || strPath.endsWith(".M3U8")) 
            m_sdkPlayer.setDRMLibrary("voDRM_VisualOn_AES128", "voGetDRMAPI");
        else 
            m_sdkPlayer.setDRMLibrary("voDRMMediaCrypto", "voGetDRMAPI"); 

        /* Open media source */
        VO_OSMP_SRC_FORMAT format = VO_OSMP_SRC_FORMAT.VO_OSMP_SRC_AUTO_DETECT;

        VO_OSMP_SRC_FLAG eSourceFlag = VO_OSMP_SRC_FLAG.VO_OSMP_FLAG_SRC_OPEN_ASYNC;

//        if (m_chbAsync != null && m_chbAsync.isChecked())
            eSourceFlag = VO_OSMP_SRC_FLAG.VO_OSMP_FLAG_SRC_OPEN_ASYNC;


        VOOSMPOpenParam openParam = null;


        openParam = new VOOSMPOpenParam();
        int type = VO_OSMP_DECODER_TYPE.VO_OSMP_DEC_VIDEO_SW.getValue() |
                   VO_OSMP_DECODER_TYPE.VO_OSMP_DEC_AUDIO_SW.getValue();
        openParam.setDecoderType(type);


        nRet = m_sdkPlayer.open(strPath, eSourceFlag, format, openParam);

        /* If Sync open */
        if (eSourceFlag == VO_OSMP_SRC_FLAG.VO_OSMP_FLAG_SRC_OPEN_SYNC) {
            /* Run (play) media pipeline */
            nRet = m_sdkPlayer.start();

            if (nRet == VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE) {
                voLog.v(TAG, "MediaPlayer run sync");
            } else {
                onError(m_sdkPlayer, getString(R.string.str_ErrOpenFail), nRet.getValue(), 0);
            }

            m_nPos = (int) m_sdkPlayer.getPosition();
            m_nDuration = (int) m_sdkPlayer.getDuration();

            updatePosDur();

            m_bStoped = false;
            m_ibPlayPause.setImageResource(R.drawable.selector_btn_pause);

        }

        m_tvCurrentTime.setText("00:00");
        m_sbMain.setProgress(0);

        // Show wait icon
        m_pbLoadingProgress.setVisibility(View.VISIBLE);

        // if needed, set initial bitrate
        //m_sdkPlayer.setInitialBitrate(500000);

        // if needed, set max buffer time
        m_sdkPlayer.setMaxBufferTime(60000);
    }
  
    private void stopVideo() {
        m_bPaused = false;
        m_bStoped = true;
        if (m_sdkPlayer != null) {
            m_sdkPlayer.stopAnalyticsNotification();

            m_sdkPlayer.stop();
            m_sdkPlayer.close();
            voLog.v(TAG, "MediaPlayer stoped.");
        }
    }

    /* Stop playback, close media source, and uninitialize SDK player */
    public void uninitPlayer() {

        if (m_sdkPlayer != null) {
            m_sdkPlayer.destroy();
            m_sdkPlayer = null;
            voLog.v(TAG, "MediaPlayer released.");
        }
    }

    private void stopAndFinish() {
        stopVideo();
        uninitPlayer();
        this.finish();
    }

    private void enableSubtitleOutput(boolean bEnable) {
        if (m_sdkPlayer != null)
            m_sdkPlayer.enableSubtitle(bEnable);
    }

    /* Display error messages and stop player */
    public boolean onError(VOCommonPlayer mp, String errStr, int what, int extra) 	{
        voLog.v(TAG, "Error message, what is " + what + " extra is " + extra);
        //String errStr = getString(R.string.str_ErrPlay_Message) + "\nError code is " + Integer.toHexString(what);

        // Dialog to display error message; stop player and exit on Back key or "OK"
        AlertDialog ad = new AlertDialog.Builder(SamplePlayerActivity.this)
                            .setIcon(R.drawable.icon)
                            .setTitle(R.string.str_ErrPlay_Title)
                            .setMessage(errStr)
                            .setOnKeyListener(new OnKeyListener() {
                                public boolean onKey(DialogInterface arg0, int arg1, KeyEvent arg2)	{
                                    if (arg1 == KeyEvent.KEYCODE_BACK) { 
                                        stopVideo();
                                        uninitPlayer();
                                        finish();
                                    } 
                                    return false;
                                }
                            })
                            .setPositiveButton(R.string.str_OK, new OnClickListener() {
                                public void onClick(DialogInterface a0, int a1)	{
                                    stopVideo();
                                    uninitPlayer();
                                    finish();
                                }
                            }).create();
        ad.show();
        return true;

    }

    // Toggle Play/Pause button display and functionality 
    private void playerPauseRestart() {
        if (m_sdkPlayer != null) {
            if(m_bPaused == false)	{
                // If playing, pause media pipeline, show media controls, and change button to "Play" icon
                m_sdkPlayer.pause();
                showMediaController();
                m_ibPlayPause.setImageResource(R.drawable.selector_btn_play);
                m_bPaused = true;
            } else {
                // Else, play media pipeline and change button to "Pause" icon
                m_sdkPlayer.start();
                m_ibPlayPause.setImageResource(R.drawable.selector_btn_pause);
                m_bPaused = false;
            }
        }
    }

    //
    // UI related methods
    //
    
    // Show media controls on activity touch event
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (m_rlBottom.getVisibility() != View.VISIBLE)
                showMediaControllerImpl();
            else 
                hideControllerImpl();
        }
        return super.onTouchEvent(event);
    }

    // Stop player and exit on Back key
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        voLog.v(TAG, "Key click is " + keyCode);

        if (keyCode ==KeyEvent.KEYCODE_BACK) {
            voLog.v(TAG, "Key click is Back key");
            stopAndFinish();
            return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    // UI for entering/selecting media source 
    private void SourceWindow() {
        LayoutInflater inflater;
        View layout;
        inflater = LayoutInflater.from(this);
        layout = inflater.inflate(R.layout.url, null);

        m_edtInputURL = (EditText)layout.findViewById(R.id.edtInputURL);
        m_spSelectURL = (Spinner)layout.findViewById(R.id.spSelectURL);
        m_spSelectSubtitle  = (Spinner)layout.findViewById(R.id.spSelectSubtitle);
        m_spSelectCustomLAURL  = (Spinner)layout.findViewById(R.id.spSelectCustomLAURL);

        m_chbAsync    = (CheckBox)layout.findViewById(R.id.chbAsync);

        // Dialog to input source URL or file path
        m_adlgMain = new AlertDialog.Builder(this)
                         .setIcon(R.drawable.icon)
                         .setTitle(R.string.str_URL)
                         .setView(layout)
                         .setNegativeButton(R.string.str_Cancel, new OnClickListener() {
                             // "Cancel" button stops player and exits
                             public void onClick(DialogInterface dialog, int which) {
                                 stopAndFinish();
                             }
                         })
                         .setPositiveButton(R.string.str_OK, new OnClickListener() {
                             // "OK" button begins playback of inputted media source
                             public void onClick(DialogInterface dialog, int which) {
                                 // editbox is prefered
                                 if (m_edtInputURL.getText().toString().length() != 0)
                                     m_strVideoPath = m_edtInputURL.getText().toString();
                                 initPlayer(m_strVideoPath);
                                 playVideo(m_strVideoPath);

                             }
                         })
                         .setOnKeyListener(new OnKeyListener() {
                             public boolean onKey(DialogInterface arg0, int arg1, KeyEvent arg2) {
                                 // "Back" button stops player and exits
                                 if (arg1 == KeyEvent.KEYCODE_BACK) {
                                     arg0.dismiss();
                                     stopAndFinish();
                                     return true;
                                 }
                                 return false;
                             }
                         })
                         .create();

        m_bSpinnerClickAutoTriggered_SelectUrl = false;
        m_bSpinnerClickAutoTriggered_SelectSubtitle = false;

        // Display media source input and selection options
        m_adlgMain.show();

        // Retrieve URL list
        m_lstSelectURL = new ArrayList<String>();
        ReadUrlInfo();

        final ArrayList<String> arrURLListWithTitle = new ArrayList<String>(m_lstSelectURL);
        arrURLListWithTitle.add(0, getResources().getString(R.string.str_SelURL_FirstLine));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arrURLListWithTitle);

        m_spSelectURL.setAdapter(adapter);
        m_spSelectURL.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // When item is selected from URL list, begin playback of selected item
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                if (m_bSpinnerClickAutoTriggered_SelectUrl == false) {
                    m_bSpinnerClickAutoTriggered_SelectUrl = true;
                    return;
                }
                voLog.v(TAG, "Id is " +  m_spSelectURL.getSelectedItemId() + ", Pos is " + m_spSelectURL.getSelectedItemPosition()
                    + ", arg2 " +  arg2 + ", arg3 " + arg3);
                m_strVideoPath = arrURLListWithTitle.get(m_spSelectURL.getSelectedItemPosition());
            }
            public void onNothingSelected(AdapterView<?> arg0){}

        });

        m_lstSelectSubtitle = new ArrayList<String>();
        ReadSubtitleInfo();

        final ArrayList<String> arrSubtitleListWithTitle = new ArrayList<String>(m_lstSelectSubtitle);
        arrSubtitleListWithTitle.add(0, getResources().getString(R.string.str_SelSubtitle_FirstLine));

        ArrayAdapter<String> adptSubtitle = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrSubtitleListWithTitle);

        m_spSelectSubtitle.setAdapter(adptSubtitle);
        m_spSelectSubtitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                if (m_bSpinnerClickAutoTriggered_SelectSubtitle == false) {
                    m_bSpinnerClickAutoTriggered_SelectSubtitle = true;
                    return;
                }

                int nPos = m_spSelectSubtitle.getSelectedItemPosition();
                m_strSubtitlePath = arrSubtitleListWithTitle.get(nPos);
            }

            public void onNothingSelected(AdapterView<?> arg0) {}
        });


        m_lstSelectCustomLAURL = new ArrayList<String>();
        ReadCustomLAUrlInfo();

        final ArrayList<String> arrCustomLAUrlListWithTitle = new ArrayList<String>(m_lstSelectCustomLAURL);
        arrCustomLAUrlListWithTitle.add(0, getResources().getString(R.string.str_SelCustomLAUrl_FirstLine));

        ArrayAdapter<String> adptCustomLAUrl = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrCustomLAUrlListWithTitle);
        m_spSelectCustomLAURL.setAdapter(adptCustomLAUrl);
        m_spSelectCustomLAURL.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                if (m_bSpinnerClickAutoTriggered_SelectCustomUrl == false) {
                    m_bSpinnerClickAutoTriggered_SelectCustomUrl = true;
                    return;
                }

                int nPos = m_spSelectCustomLAURL.getSelectedItemPosition();
                m_LA_URL = arrCustomLAUrlListWithTitle.get(nPos);
            }

            public void onNothingSelected(AdapterView<?> arg0) {}
        });
    }

    // Show media controller. called by handler 
    private void showMediaController() {
        if(m_sdkPlayer == null)
            return;
        handler.sendEmptyMessage(MSG_SHOW_CONTROLLER);
    }

    // Hide media controller. Called by handler 
    public void hideController() {
        handler.sendEmptyMessage(MSG_HIDE_CONTROLLER);
    }

    // Show media controller (implementation)
    private void showMediaControllerImpl()	{
        voLog.v(TAG, "Touch screen, layout status is " +m_rlBottom.getVisibility());
        m_dateUIDisplayStartTime = new Date(System.currentTimeMillis());		// Get current system time

        if (m_rlBottom.getVisibility() != View.VISIBLE) {
            voLog.v(TAG, "mIsStop is " + m_bStoped);

            // Schedule next UI update in 200 milliseconds
            if(m_ttMain != null)
                m_ttMain = null;

            m_ttMain= new TimerTask() {
                public void run() {
                    handler.sendEmptyMessage(MSG_UPDATE_UI);
                }
            };

            if(m_timerMain == null)
                m_timerMain = new Timer();

            m_timerMain.schedule(m_ttMain, 0, 200);

            // Show controls
            m_rlBottom.setVisibility(View.VISIBLE);
            m_rlTop.setVisibility(View.VISIBLE);

            voLog.v(TAG, "m_rlTop show " + m_rlTop.getVisibility());
        }
    }

    // Hide media controller (implementation)
    public void hideControllerImpl() {
        if(m_timerMain != null)	{
            m_timerMain.cancel();
            m_timerMain.purge();
            m_timerMain = null;
            m_ttMain = null;
        }

        m_rlBottom.setVisibility(View.INVISIBLE);
        m_rlTop.setVisibility(View.INVISIBLE);
    }

    private void doUpdateUI() {
        // If the controls are not visible do not update
        if(m_rlBottom.getVisibility() != View.VISIBLE)
            return;

        /* If the player is stopped do not update */
        if (m_bStoped)
            return;

        /* If the user is dragging the progress bar do not update */
        if (m_bTrackProgressing)
            return;

        Date timeNow = new Date(System.currentTimeMillis());
        long timePeriod = (timeNow.getTime() - m_dateUIDisplayStartTime.getTime())/1000;
        if (timePeriod >= 10 && m_bPaused == false
            && m_rlProgramInfo.getVisibility() != View.VISIBLE ) {
            /* If the media is being played back, hide media controls after 3 seconds if unused */
            hideController();
            return;
        }

        /* update the Seekbar and Time display with current position */
        m_nPos = (int) m_sdkPlayer.getPosition();
        if (!m_sdkPlayer.canBePaused()) { // live mode

            long lMinPos = m_sdkPlayer.getMinPosition();
            long lMaxPos = m_sdkPlayer.getMaxPosition();
            if (lMinPos == 0 && lMaxPos == 0)  
                return;

            // DVR
            if (m_tvCurrentTime.getVisibility() == View.VISIBLE)
                m_tvCurrentTime.setVisibility(View.INVISIBLE);
            if (m_tvTotalTime.getVisibility() == View.VISIBLE)
                m_tvTotalTime.setVisibility(View.INVISIBLE);

            int nDuration = (int) (m_sdkPlayer.getMaxPosition() - m_sdkPlayer.getMinPosition());
            int nPos = (int) (m_nPos - m_sdkPlayer.getMinPosition());

            if (nDuration > 0)
                m_sbMain.setProgress(100 * nPos/ nDuration);

        } else if (m_nDuration > 0) {                         // vod mode

            m_sbMain.setProgress(100 * m_nPos/ m_nDuration);
            String str = DateUtils.formatElapsedTime(m_nPos/1000);
            m_tvCurrentTime.setText(str);
        }
        
        int validBufferDuation = m_sdkPlayer.getValidBufferDuration();
        voLog.i(TAG, "The valid buffer left can be used to playback is "+validBufferDuation);
        VO_OSMP_DOWNLOAD_STATUS audioDownloadStatus = m_sdkPlayer.getDownloadStatus(VO_OSMP_SOURCE_STREAMTYPE.VO_OSMP_SS_AUDIO);
        VO_OSMP_DOWNLOAD_STATUS videoDownloadStatus = m_sdkPlayer.getDownloadStatus(VO_OSMP_SOURCE_STREAMTYPE.VO_OSMP_SS_VIDEO);
        VO_OSMP_DOWNLOAD_STATUS subtitleDownloadStatus = m_sdkPlayer.getDownloadStatus(VO_OSMP_SOURCE_STREAMTYPE.VO_OSMP_SS_SUBTITLE);
        voLog.i(TAG, "The video download status is " + videoDownloadStatus + ", the audio download status is " + audioDownloadStatus + ", the subtitle download status is " + subtitleDownloadStatus + ".");
    }

    private void updatePosDur() {
        if (!m_sdkPlayer.canBePaused()) { // live
            m_tvCurrentTime.setTextColor(getResources().getColor(R.color.darkgray));
            m_tvTotalTime.setTextColor(getResources().getColor(R.color.darkgray));
            m_tvTotalTime.setText("00:00");
            m_sbMain.setEnabled(false);
        } else {                          // vod
            m_tvCurrentTime.setTextColor(getResources().getColor(android.R.color.white));
            m_tvTotalTime.setTextColor(getResources().getColor(android.R.color.white));
            m_tvTotalTime.setText(DateUtils.formatElapsedTime(m_nDuration / 1000));
            m_sbMain.setEnabled(true);
        }
    }
	
	/* Retrieve list of media sources */
    private void ReadUrlInfo() {
        voLog.i(TAG, "Current external storage directory is %s", Environment.getExternalStorageDirectory().getAbsolutePath());
        String str = Environment.getExternalStorageDirectory().getAbsolutePath() + "/url.txt";
        if (CommonFunc.ReadUrlInfoToList(m_lstSelectURL, str) == false)
            Toast.makeText(this, "Could not find " + str, Toast.LENGTH_LONG).show();
    }
    
    private void ReadSubtitleInfo() {
        String str = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ttml_url.txt";
        if (CommonFunc.ReadUrlInfoToList(m_lstSelectSubtitle, str) == false) {
            //Toast.makeText(this, "Could not find " + str, Toast.LENGTH_LONG).show();
        }
    }

    private void ReadCustomLAUrlInfo() {
        String str = Environment.getExternalStorageDirectory().getAbsolutePath() + "/LA_URL.txt";
        if (CommonFunc.ReadUrlInfoToList(m_lstSelectCustomLAURL, str) == false) {
            //Toast.makeText(this, "Could not find " + str, Toast.LENGTH_LONG).show();
        }
    }

    /* Manage SDK requests */
    public int onRequest(int arg0, int arg1, int arg2, Object arg3) {
        voLog.i(TAG, "onRequest arg0 is "+ arg0);
        return 0;
    }

    private void resetUIDisplayStartTime() {
        m_dateUIDisplayStartTime = new Date(System.currentTimeMillis());
    }

    private void initUI() {
        initLayout();
        initLayoutRight();
        initLayoutLeft();
    }

    private void initLayout() {
        m_rlTop = (TableLayout) findViewById(R.id.tlTop);
        m_rlBottom = (RelativeLayout) findViewById(R.id.rlBottom);
    }
    
    private void disablePopupMenuLableClick() {
        ((TextView) findViewById(R.id.tvVideo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { /* do nothing */ }});
        
        ((TextView) findViewById(R.id.tvAudio)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { /* do nothing */ }});
        
        ((TextView) findViewById(R.id.tvSubtitle)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { /* do nothing */ }});
    }

    private void initLayoutRight() {
        m_rlRight = (RelativeLayout) findViewById(R.id.rlRight);

        m_hsvRight= (HorizontalScrollView) findViewById(R.id.hsvRight);
        m_llRight = (LinearLayout) findViewById(R.id.llRight);

        m_rlProgramInfo = (RelativeLayout) findViewById(R.id.rlProgramInfo);
        m_rlProgramInfo.setVisibility(View.INVISIBLE);
        
        m_rlProgramInfoArrow = (RelativeLayout) findViewById(R.id.rlProgramInfoArrow);
        m_rlProgramInfoArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetUIDisplayStartTime();
                if (m_rlProgramInfo.getVisibility() != View.VISIBLE) {
                    m_rlProgramInfo.setVisibility(View.VISIBLE);
                    m_rlProgramInfoArrow.setBackgroundResource(R.drawable.bg_player_right_programinfo_on);
                    fillProgramInfo();
                } else {
                    m_rlProgramInfo.setVisibility(View.INVISIBLE);
                    m_rlProgramInfoArrow.setBackgroundResource(R.drawable.bg_player_right_programinfo_off);
                }

            }
        });
        
        if (!m_bEnableVideo && !m_bEnableAudio && !m_bEnableSubtitle)
            m_rlProgramInfoArrow.setVisibility(View.INVISIBLE);
        else {
            if (!m_bEnableVideo)
                findViewById(R.id.rlVideo).setVisibility(View.GONE);
            if (!m_bEnableAudio)
                findViewById(R.id.rlAudio).setVisibility(View.GONE);
            if (!m_bEnableSubtitle)
                findViewById(R.id.rlSubtitle).setVisibility(View.GONE);
        }
        
        disablePopupMenuLableClick();
        
        m_tvCommit = (TextView) findViewById(R.id.tvCommit);
        m_tvCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               
                VO_OSMP_RETURN_CODE ret = m_sdkPlayer.commitSelection();
          
                if (ret == VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE) {
                    
                    // UI logic
                    resetProgramInfoPopupMenu();

                    resetTextAppearance();

                    // use SelectedXXXindex
                    initProgramInfoPopupMenu(m_nSelectedVideoIndex,
                            m_nSelectedAudioIndex, m_nSelectedSubtitleIndex);
                
                } else {
                    voLog.e(TAG, "commitSelection returns: " + Integer.toHexString(ret.getValue()));
                }
                
            }
        });
        
        m_tvRevert = (TextView) findViewById(R.id.tvRevert);
        m_tvRevert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // data logic
                VO_OSMP_RETURN_CODE ret = m_sdkPlayer.clearSelection();
                
                if (ret == VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE) {
                    
                    // UI logic
                    resetProgramInfoPopupMenu();
                    
                    resetTextAppearance();
                    
                    requirePlayingAssetIndex();
                    
                    // use PlayingXXXindex
                    int nVideoIndex = m_nPlayingVideoIndex + 1;
                    if (m_nPlayingVideoIndex == VOCommonPlayerAssetSelection.VO_OSMP_ASSET_AUTO_SELECTED)
                        nVideoIndex = 0;
                    
                    initProgramInfoPopupMenu(nVideoIndex,
                            m_nPlayingAudioIndex, m_nPlayingSubtitleIndex);
                
                } else {
                    voLog.e(TAG, "clearSelection returns: " + Integer.toHexString(ret.getValue()));
                }
                
            }
        });
        
        
        initLayoutRightPopupMenu();
        
        // reserved/extended feature
        m_hsvRight.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                resetUIDisplayStartTime();
                return false;
            }
        });

        m_llRight.setOnTouchListener(new OnTouchListener() {

            public boolean onTouch(View view, MotionEvent motionevent) {

                if (motionevent.getAction() == MotionEvent.ACTION_DOWN) {

                    if (m_rlBottom.getVisibility() == View.VISIBLE) {
                        hideControllerImpl();
                        return true;
                    }
                }

                return false;
            }
        });
        return;
    }
  
    private LinearLayout requirePopupMenuLayout(AssetType type) {
        if (type == AssetType.Asset_Video)
            return m_llVideoPopupMenu;
        else if (type == AssetType.Asset_Audio)
            return m_llAudioPopupMenu;
        else if (type == AssetType.Asset_Subtitle)
            return m_llSubtitlePopupMenu;
        
        return null;
    }
    
    private void initProgramInfoPopupMenu(AssetType type, int nIndex) {
        LinearLayout llParent = requirePopupMenuLayout(type);
        
        if (llParent == null || nIndex >= llParent.getChildCount() || 
                nIndex < 0)
            return;
        
        LinearLayout llChild = (LinearLayout) llParent.getChildAt(nIndex);

        TextView tv = (TextView) llChild.findViewById(R.id.tvContent);
        tv.setSelected(true);
    }
    
    private void requirePlayingAssetIndex() {
        VOOSMPAssetIndex objAsset = m_sdkPlayer.getPlayingAsset();

        if (objAsset != null)
            voLog.e(TAG, "getPlayingAsset returns: NULL");
        
        m_nPlayingVideoIndex = objAsset.getVideoIndex();
        m_nPlayingAudioIndex = objAsset.getAudioIndex();
        m_nPlayingSubtitleIndex = objAsset.getSubtitleIndex();
        
        if (m_nPlayingAudioIndex < 0)
            voLog.e(TAG, "m_nPlayingAudioIndex : " + m_nPlayingAudioIndex);
        if (m_nPlayingSubtitleIndex < 0)
            voLog.e(TAG, "m_nPlayingSubtitleIndex : " + m_nPlayingSubtitleIndex);
    }
    
    private void initProgramInfoPopupMenu(int nVideoIndex, int nAudioIndex, int nSubtitleIndex) {
        initProgramInfoPopupMenu(AssetType.Asset_Video, nVideoIndex);
        initProgramInfoPopupMenu(AssetType.Asset_Audio, nAudioIndex);
        initProgramInfoPopupMenu(AssetType.Asset_Subtitle, nSubtitleIndex);
    }
    
    private void resetProgramInfoPopupMenu(LinearLayout llParent, int nIndex) {
        if (llParent == null || nIndex >= llParent.getChildCount()
                || nIndex < 0)
            return;
        
        LinearLayout llChild = (LinearLayout) llParent.getChildAt(nIndex);

        TextView tv = (TextView) llChild.findViewById(R.id.tvContent);
        tv.setSelected(false);
        tv.setEnabled(true);
    }
    
    private void resetProgramInfoPopupMenu(AssetType type) {
        LinearLayout llParent = requirePopupMenuLayout(type);
        
        if (llParent == null)
            return;
        
        for (int nIndex = 0; nIndex < llParent.getChildCount(); nIndex++)
            resetProgramInfoPopupMenu(llParent, nIndex);
    }
    
    private void resetProgramInfoPopupMenu() {
        resetProgramInfoPopupMenu(AssetType.Asset_Video);
        resetProgramInfoPopupMenu(AssetType.Asset_Audio);
        resetProgramInfoPopupMenu(AssetType.Asset_Subtitle);
    }
    
    private void disableProgramInfoPopupMenu(AssetType type, int nIndex) {
        
        LinearLayout llParent = requirePopupMenuLayout(type);
        
        if (llParent == null || nIndex >= llParent.getChildCount()
                || nIndex < 0)
            return;
        
        LinearLayout llChild = (LinearLayout) llParent.getChildAt(nIndex);

        TextView tv = (TextView) llChild.findViewById(R.id.tvContent);
        tv.setSelected(false);
        tv.setEnabled(false);
        
    }
    
    private final int INDEX_RESETALL = Integer.MAX_VALUE;
    
    private void resetTextAppearance(AssetType type, int nIndexExcept)  {
        LinearLayout llParent = requirePopupMenuLayout(type);
        if (llParent == null)
            return;
        
        if (nIndexExcept != INDEX_RESETALL) 
            if (nIndexExcept >= llParent.getChildCount() || nIndexExcept < 0)
                return;
        
        for (int i = 0; i < llParent.getChildCount(); i++) {
     
            LinearLayout llChild;
            TextView tv;
                
            if (nIndexExcept == INDEX_RESETALL) {
                  
                llChild = (LinearLayout) llParent.getChildAt(i);
                tv = (TextView) llChild.findViewById(R.id.tvContent);
                    
            } else {
                    
                if (i == nIndexExcept)
                    continue;
                  
                llChild = (LinearLayout) llParent.getChildAt(i);
                tv = (TextView) llChild.findViewById(R.id.tvContent);
                  
            }
                
            tv.setTextAppearance(getApplicationContext(),
                    R.style.style_player_textview_general_selector);
        }
       
    }
    
    private void resetTextAppearance() {
        resetTextAppearance(AssetType.Asset_Video, INDEX_RESETALL);
        resetTextAppearance(AssetType.Asset_Audio, INDEX_RESETALL);
        resetTextAppearance(AssetType.Asset_Subtitle, INDEX_RESETALL);
    }
    
    private void setProgramInfoAssetEventListener(final TextView tv, final AssetType type) {
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv.setTextAppearance(getApplicationContext(),
                        R.style.style_player_textview_general_highlight);
                
                VO_OSMP_RETURN_CODE ret;
                
                int nIndex = ((Integer) view.getTag()).intValue();
                
                if (type == AssetType.Asset_Video) {

                    int nVideoIndex;
                    
                    if (nIndex == 0)
                        nVideoIndex = VOCommonPlayerAssetSelection.VO_OSMP_ASSET_AUTO_SELECTED;
                    else 
                        nVideoIndex = nIndex - 1; 
                    
                    // data logic
                    ret = m_sdkPlayer.selectVideo(nVideoIndex);
                    if (ret != VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE) {
                        voLog.e(TAG, "selectVideo returns: " + Integer.toHexString(ret.getValue()));
                        return;
                    }
                    
                    m_nSelectedVideoIndex = nIndex; // not nVideoIndex;
                    
                    // UI logic
                    resetTextAppearance(AssetType.Asset_Video, nIndex);
                    
                    for (int i = 0; i < m_sdkPlayer.getAudioCount(); i++) { 
                        if (!m_sdkPlayer.isAudioAvailable(i))
                            disableProgramInfoPopupMenu(AssetType.Asset_Audio, i);
                    }
                    
                    for (int i = 0; i < m_sdkPlayer.getSubtitleCount(); i++) { 
                        if (!m_sdkPlayer.isSubtitleAvailable(i))
                            disableProgramInfoPopupMenu(AssetType.Asset_Subtitle, i);
                    }
                    
                } else if (type == AssetType.Asset_Audio) {
                    // data logic
                    ret = m_sdkPlayer.selectAudio(nIndex);
                    if (ret != VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE) {
                        voLog.e(TAG, "selectAudio returns: " + Integer.toHexString(ret.getValue()));
                        return;
                    }
                    
                    m_nSelectedAudioIndex = nIndex;
                    
                    // UI logic
                    resetTextAppearance(AssetType.Asset_Audio, nIndex);
                    
                    for (int i = 0; i < m_sdkPlayer.getVideoCount(); i++) { 
                        if (!m_sdkPlayer.isVideoAvailable(i))
                            //  0 is video auto, so use i + 1
                            disableProgramInfoPopupMenu(AssetType.Asset_Video, i + 1);
                    }
                    
                    for (int i = 0; i < m_sdkPlayer.getSubtitleCount(); i++) { 
                        if (!m_sdkPlayer.isSubtitleAvailable(i))
                            disableProgramInfoPopupMenu(AssetType.Asset_Subtitle, i);
                    }
                  
                } else if (type == AssetType.Asset_Subtitle) {
                    // data logic
                    ret = m_sdkPlayer.selectSubtitle(nIndex);
                    if (ret != VO_OSMP_RETURN_CODE.VO_OSMP_ERR_NONE) {
                        voLog.e(TAG, "selectSubtitle returns: " + Integer.toHexString(ret.getValue()));
                        return;
                    }
                    
                    m_nSelectedSubtitleIndex = nIndex;
                    
                    // UI logic
                    resetTextAppearance(AssetType.Asset_Subtitle, nIndex);
                    
                    for (int i = 0; i < m_sdkPlayer.getVideoCount(); i++) { 
                        if (!m_sdkPlayer.isVideoAvailable(i))
                            //  0 is video auto, so use i + 1
                            disableProgramInfoPopupMenu(AssetType.Asset_Video, i + 1);
                    }
                    
                    for (int i = 0; i < m_sdkPlayer.getAudioCount(); i++) { 
                        if (!m_sdkPlayer.isAudioAvailable(i))
                            disableProgramInfoPopupMenu(AssetType.Asset_Audio, i);
                    }
                }
            } });
    }
    
    @SuppressWarnings("unchecked")
    private void inflatePopupMenu(Context context, final LinearLayout parentView,
            ArrayList<?> lstT, final AssetType type) {

        if (lstT.size() == 0)
            return;

        LinearLayout llBuffer;
        TextView tvBuffer;

        int size = lstT.size();
        if (size <= 3) {
            // There is a bug in HorizontalScrollView, so we have to use a workaround.
            // http://stackoverflow.com/questions/9031817/android-horizontalscrollview-with-right-layout-gravity-working-wrong
            HorizontalScrollView.LayoutParams params = new HorizontalScrollView.LayoutParams( 
                    HorizontalScrollView.LayoutParams.WRAP_CONTENT, HorizontalScrollView.LayoutParams.FILL_PARENT); 
            params.gravity = Gravity.RIGHT;
            parentView.setLayoutParams(params);
        }else{
            HorizontalScrollView.LayoutParams params = new HorizontalScrollView.LayoutParams( 
                    HorizontalScrollView.LayoutParams.WRAP_CONTENT, HorizontalScrollView.LayoutParams.FILL_PARENT); 
            params.gravity = Gravity.LEFT;
            parentView.setLayoutParams(params);
        }
        
        for (int i = 0; i < size; i++) {
            if (i == 0) {
                llBuffer = (LinearLayout)LayoutInflater.from(context)
                    .inflate(R.layout.player_popupmenu_left, parentView, false);
                
            } else {
                llBuffer = (LinearLayout)LayoutInflater.from(context)
                    .inflate(R.layout.player_popupmenu_normal, parentView, false);
            }

            parentView.addView(llBuffer);

            tvBuffer = (TextView) llBuffer.findViewById(R.id.tvContent);
            
            tvBuffer.setText(((ArrayList<String>) lstT).get(i));
            tvBuffer.setTag(i);
            
            setProgramInfoAssetEventListener(tvBuffer, type);
                    
        }
    }

    // Right layout functions
    private void initLayoutRightPopupMenu() {
        m_llVideoPopupMenu = (LinearLayout) findViewById(R.id.llVideoPopupMenu);
        m_llAudioPopupMenu = (LinearLayout) findViewById(R.id.llAudioPopupMenu);
        m_llSubtitlePopupMenu = (LinearLayout) findViewById(R.id.llSubtitlePopupMenu);
        
        m_hsvVideoPopupMenu = (HorizontalScrollView) findViewById(R.id.hsvVideoPopupMenu);
        m_hsvVideoPopupMenu.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionevent) {
                if (motionevent.getAction() == MotionEvent.ACTION_MOVE)
                    resetUIDisplayStartTime();

                return false;
            }
        });

        m_hsvAudioPopupMenu = (HorizontalScrollView) findViewById(R.id.hsvAudioPopupMenu);
        m_hsvAudioPopupMenu.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionevent) {
                if (motionevent.getAction() == MotionEvent.ACTION_MOVE)
                    resetUIDisplayStartTime();

                return false;
            }
        });
        
        m_hsvSubtitlePopupMenu = (HorizontalScrollView) findViewById(R.id.hsvSubtitlePopupMenu);
        m_hsvSubtitlePopupMenu.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionevent) {
                if (motionevent.getAction() == MotionEvent.ACTION_MOVE)
                    resetUIDisplayStartTime();

                return false;
            }
        });
    }

    private void getVideoDescription(ArrayList<String> lstString) {
        if (lstString == null || m_sdkPlayer == null)
            return;
        
        int nAssetCount = m_sdkPlayer.getVideoCount();
        if (nAssetCount == 0) 
            return;
        
        int nDefaultIndex = 0;
        
        for (int nAssetIndex = 0; nAssetIndex < nAssetCount; nAssetIndex++) {
          
            VOOSMPAssetProperty propImpl = m_sdkPlayer.getVideoProperty(nAssetIndex);
            
            String strDescription;
            
            int nPropertyCount = propImpl.getPropertyCount();
            if (nPropertyCount == 0) {
                strDescription = STRING_ASSETPROPERTYNAME_VIDEO + Integer.toString(nDefaultIndex++);
            } else {
                final int KEY_DESCRIPTION_INDEX = 2;
                strDescription = (String) propImpl.getValue(KEY_DESCRIPTION_INDEX);
            }
            lstString.add(strDescription);
        }
    }
    
    private void getAudioDescription(ArrayList<String> lstString) {
        if (lstString == null || m_sdkPlayer == null)
            return;
        
        int nAssetCount = m_sdkPlayer.getAudioCount();
        if (nAssetCount == 0) 
            return;
        
        int nDefaultIndex = 0;
        
        for (int nAssetIndex = 0; nAssetIndex < nAssetCount; nAssetIndex++) {
            VOOSMPAssetProperty propImpl = m_sdkPlayer.getAudioProperty(nAssetIndex);
            String strDescription;
            int nPropertyCount = propImpl.getPropertyCount();
            if (nPropertyCount == 0) {
                strDescription = STRING_ASSETPROPERTYNAME_AUDIO + Integer.toString(nDefaultIndex++);
            } else {
                final int KEY_DESCRIPTION_INDEX = 1;
                strDescription = (String) propImpl.getValue(KEY_DESCRIPTION_INDEX);
            }
            lstString.add(strDescription);
        }
    }
    
    private void getSubtitleDescription(ArrayList<String> lstString) {
        
        if (lstString == null || m_sdkPlayer == null)
            return;
        
        int nAssetCount = m_sdkPlayer.getSubtitleCount();
        if (nAssetCount == 0) 
            return;
        
        int nDefaultIndex = 0;
        
        for (int nAssetIndex = 0; nAssetIndex < nAssetCount; nAssetIndex++) {
            VOOSMPAssetProperty propImpl = m_sdkPlayer.getSubtitleProperty(nAssetIndex);
            String strDescription;
            
            int nPropertyCount = propImpl.getPropertyCount();
            if (nPropertyCount == 0) {
                strDescription = STRING_ASSETPROPERTYNAME_SUBTITLE + Integer.toString(nDefaultIndex++);
            } else {
                final int KEY_DESCRIPTION_INDEX = 1;
                strDescription = (String) propImpl.getValue(KEY_DESCRIPTION_INDEX);
            }
            
            lstString.add(strDescription);
        }
    }

    private void fillAssetInfo(ArrayList<String> lstString,
            LinearLayout llPopupMenu, AssetType type) {

        llPopupMenu.removeAllViews();
        inflatePopupMenu(SamplePlayerActivity.this, llPopupMenu, lstString, type);
        
        int nIndex = 0;
        if (type == AssetType.Asset_Video) {
            if (m_nPlayingVideoIndex == VOCommonPlayerAssetSelection.VO_OSMP_ASSET_AUTO_SELECTED)
                nIndex = 0;
            else 
                nIndex = m_nPlayingVideoIndex + 1;
        }
        else if (type == AssetType.Asset_Audio)
            nIndex = m_nPlayingAudioIndex;
        else if (type == AssetType.Asset_Subtitle)
            nIndex = m_nPlayingSubtitleIndex;
            
        initProgramInfoPopupMenu(type, nIndex);
        
        llPopupMenu.scheduleLayoutAnimation();
    }

    private void fillProgramInfo() {
        resetUIDisplayStartTime();
        requirePlayingAssetIndex();
        
        ArrayList<String> lstVideo = new ArrayList<String>();
        getVideoDescription(lstVideo);
        lstVideo.add(0, getResources().getString(R.string.Player_BpsQuality_Auto));

        fillAssetInfo(lstVideo, m_llVideoPopupMenu, AssetType.Asset_Video);

        ArrayList<String> lstAudio = new ArrayList<String>();
        getAudioDescription(lstAudio);

        fillAssetInfo(lstAudio, m_llAudioPopupMenu, AssetType.Asset_Audio);

        ArrayList<String> lstSubtitle = new ArrayList<String>();
        getSubtitleDescription(lstSubtitle);

        fillAssetInfo(lstSubtitle, m_llSubtitlePopupMenu, AssetType.Asset_Subtitle);
    }
    
    void initLayoutLeft() {
        m_rlChannel = (RelativeLayout) findViewById(R.id.rlChannel);
        m_lvChannel = (ListView) findViewById(R.id.lvChannel);

        if (m_bEnableChannel) {
            m_rlChannel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resetUIDisplayStartTime();
                    if (m_lvChannel.getVisibility() == View.INVISIBLE) {
                        m_lvChannel.setVisibility(View.VISIBLE);
                        m_rlChannel.setBackgroundResource(R.drawable.bg_player_left_channel_on);
                        fillChannelListContent();
                    } else {
                        m_lvChannel.setVisibility(View.INVISIBLE);
                        m_rlChannel.setBackgroundResource(R.drawable.bg_player_left_channel_off);
                    }

                }
            });
        } else {
            m_rlChannel.setVisibility(View.GONE);
            m_lvChannel.setVisibility(View.GONE);
        }

    }

    private void fillChannelListContent() {

        if (m_lvChannel.getAdapter() == null) {
            if (m_lstSelectURL == null) {
                m_lstSelectURL= new ArrayList<String>();
                ReadUrlInfo();
            }

            ArrayList<HashMap<String, String>> urlList = new ArrayList<HashMap<String, String>>();
            for (int i = 0; i < m_lstSelectURL.size(); i++) {
                HashMap<String, String> urlHash = new HashMap<String, String>();
                urlHash.put("url", m_lstSelectURL.get(i));
                urlList.add(urlHash);
            }

            m_lvChannel.setAdapter(new SimpleAdapter(this, urlList,
                        R.layout.simplelistitem1, new String[] { "url" },
                        new int[] { R.id.tvUrl }));

            m_lvChannel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1,
                    int arg2, long arg3) {
                    stopVideo();
                    playVideo(m_lstSelectURL.get(arg2));
                }
            });
        }
    }
	
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Version");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle() == "Version") {
            
            View layoutVersion = LayoutInflater.from(SamplePlayerActivity.this).inflate(R.layout.version, null);
            ListView lvVersion = (ListView) layoutVersion.findViewById(R.id.lvVersion);
            ArrayList<HashMap<String, String>> lstVersion = new ArrayList<HashMap<String, String>>();
			      HashMap<String, String> hashmapVersion = new HashMap<String, String>();
            
				try {
				
						PackageManager pm = getPackageManager();
						PackageInfo pinfo = pm.getPackageInfo(getPackageName(), PackageManager.GET_CONFIGURATIONS);
						String versionCode = pinfo.versionName;
						int versionName = pinfo.versionCode;
						
						hashmapVersion.put("version", "Build Release: V" + versionCode + "." + versionName);
						lstVersion.add(hashmapVersion);
				} catch (NameNotFoundException e) {
				}
			 
            AssetManager am = getResources().getAssets();
            try {
                InputStream is = am.open("libversions.txt");
               
                byte[] arrContent = new byte[32*1024];
                is.read(arrContent);
                String strVersion = new String(arrContent);
                String[] strlstVersion = strVersion.split("[\\n]");
                
                for (int i = 0; i < strlstVersion.length; i++) {
                   if (strlstVersion[i].indexOf("voAbout> ") == -1)
                       continue;
                   strlstVersion[i] = strlstVersion[i].substring(strlstVersion[i].indexOf("voAbout> ") + 9);
                }
                
                for (int i = 0; i < strlstVersion.length; i++) {
                    hashmapVersion = new HashMap<String, String>();
                    hashmapVersion.put("version", strlstVersion[i]);
                    lstVersion.add(hashmapVersion);
                }
                
            } catch (IOException e) {
            }
            
            lvVersion.setAdapter(new SimpleAdapter(this, lstVersion, R.layout.simplelistitem1,
                        new String[] { "version" }, new int[] { R.id.tvUrl }));
            
            AlertDialog adlgVersion = new AlertDialog.Builder(SamplePlayerActivity.this)
                    .setTitle("Version").setView(layoutVersion)
                    .setPositiveButton(R.string.str_OK, new OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).create();

            adlgVersion.show();
        }
        
        return super.onOptionsItemSelected(item);
    }

    private void updateStatusViewText() {
        String str = m_nVideoWidth + "x" + m_nVideoHeight;
/*
        if (m_nBitRate > 0) {
            str += "\n";
            if (m_nBitRate > 1000000) 
                str += ((float)m_nBitRate/1000000) + " Mbps";
            else if (m_nBitRate > 1000) 
                str += ((float)m_nBitRate/1000) + " Kbps";
            else 
                str += m_nBitRate + " bps";
        }
*/
        m_statusView.setText(str);
    }
}
