package com.accedo.firetv.player.module;

import android.content.res.Resources;

import static com.accedo.firetv.player.module.ApplicationModule.applicationContext;


public class ResourcesModule {

	public static Resources resources() {
		return applicationContext().getResources();
	}
}
