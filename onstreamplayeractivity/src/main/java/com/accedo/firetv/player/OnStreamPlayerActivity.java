package com.accedo.firetv.player;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.accedo.firetv.player.playback.VideoPlayerWrapper;
import com.accedo.firetv.player.playback.VideoPlayerWrapperListener;

import static com.accedo.firetv.player.module.HandlerModule.mainThreadHandler;
import static com.accedo.firetv.player.module.playback.VideoPlayerWrapperModule.videoPlayerWrapper;


public class OnStreamPlayerActivity extends Activity implements SurfaceHolder.Callback, VideoPlayerWrapperListener,
        DialogInterface.OnDismissListener, OnClickListener, SeekBar.OnSeekBarChangeListener, View.OnClickListener {

    private static final int UPDATE_PROGRESS_DELAY = 200;
    private static final long DISMISS_BARS_DELAY = 10000;
    private static final String PARAM_URL = "url";
    private static final String PARAM_LICENSE_URL = "licenseUrl";
    private static final String PARAM_VIDEO_TITLE = "videoTitle";

    private final VideoPlayerWrapper videoPlayerWrapper;
    private final Handler handler;

    private SurfaceView surfaceView;
    private View topBar;
    private View bottomBar;
    private ImageView playPauseButton;
    private SeekBar seekBar;
    private TextView currentTimeText;
    private TextView totalTimeText;
    private TextView statusText;

    private boolean isSeeking;

    private Runnable updateProgressBarRunnable = new Runnable() {
        @Override
        public void run() {
            updateProgress();
            handler.postDelayed(this, UPDATE_PROGRESS_DELAY);
        }
    };
    private Runnable dismissBarsRunnable = new Runnable() {
        @Override
        public void run() {
            dismissBars();
        }
    };

    public static Intent getOnStreamPlayerIntent(Context context, String url, String licenseUrl, String videoTitle) {
        Intent intent = new Intent(context, OnStreamPlayerActivity.class);
        intent.putExtra(PARAM_URL, url);
        intent.putExtra(PARAM_LICENSE_URL, licenseUrl);
        intent.putExtra(PARAM_VIDEO_TITLE, videoTitle);
        return intent;
    }

    public OnStreamPlayerActivity() {
        this(videoPlayerWrapper(), mainThreadHandler());
    }

    public OnStreamPlayerActivity(VideoPlayerWrapper videoPlayerWrapper, Handler handler) {
        this.videoPlayerWrapper = videoPlayerWrapper;
        this.handler = handler;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_player);
        getWindow().setFormat(PixelFormat.UNKNOWN);
        findViews();
        initializeViews();
    }

    private void initializeViews() {
        surfaceView.getHolder().addCallback(this);
        surfaceView.getHolder().setFormat(PixelFormat.RGBA_8888);
        seekBar.setOnSeekBarChangeListener(this);
        playPauseButton.setOnClickListener(this);
        topBar.setVisibility(View.VISIBLE);
        bottomBar.setVisibility(View.VISIBLE);
        statusText.setText(R.string.initializing);
    }

    private void findViews() {
        surfaceView = (SurfaceView) findViewById(R.id.main_surfaceview);
        topBar = findViewById(R.id.top_video_bar);
        bottomBar = findViewById(R.id.bottom_video_bar);
        playPauseButton = (ImageView) findViewById(R.id.play_pause_image);
        seekBar = (SeekBar) findViewById(R.id.seekbar);
        currentTimeText = (TextView) findViewById(R.id.current_time_text);
        totalTimeText = (TextView) findViewById(R.id.total_time_text);
        statusText = (TextView) findViewById(R.id.status_text);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceholder) {
        OnStreamLogger.i("Surface Created");
        Intent intent = getIntent();
        String videoPath = intent.getStringExtra(PARAM_URL);
        String licenseServer = intent.getStringExtra(PARAM_LICENSE_URL);
        videoPlayerWrapper.initialize(videoPath, licenseServer, surfaceView, this);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceholder, int format, int w, int h) {
        OnStreamLogger.i("Surface Changed");
        videoPlayerWrapper.surfaceChanged();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceholder) {
        OnStreamLogger.i("Surface Destroyed");
        handler.removeCallbacks(updateProgressBarRunnable);
        handler.removeCallbacks(dismissBarsRunnable);
        videoPlayerWrapper.releaseMediaPlayer();
    }

    //TODO: improve this error handling
    @Override
    public void onError(int what, int extra) {
        OnStreamLogger.w("Error message, what is " + what + " extra is " + extra);
        String errStr = getString(R.string.error_message) + "\n Error code is " + Integer.toHexString(what);
        AlertDialog ad = new AlertDialog.Builder(this)
                .setIcon(R.drawable.icon)
                .setTitle(R.string.error)
                .setMessage(errStr)
                .setOnDismissListener(this)
                .setPositiveButton(R.string.ok, this)
                .create();
        ad.show();
        handler.removeCallbacks(updateProgressBarRunnable);
    }

    @Override
    public void onOpenSuccess() {
        // Start timer to update seekbar
        handler.post(updateProgressBarRunnable);
        handler.postDelayed(dismissBarsRunnable, DISMISS_BARS_DELAY);
    }

    @Override
    public void onVideoSizeChanged(int width, int height) {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
            ViewGroup.LayoutParams lp = surfaceView.getLayoutParams();
            lp.width = dm.widthPixels;
            lp.height = dm.widthPixels * height / width;
            surfaceView.setLayoutParams(lp);
    }

    @Override
    public void onPlaybackComplete() {
        finish();
    }

    @Override
    public void onBufferStart() {
        statusText.setText(R.string.buffering);
    }

    @Override
    public void onBufferStop() {
        statusText.setText(getString(R.string.now_playing, getIntent().getStringExtra(PARAM_VIDEO_TITLE)));
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        finish();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        OnStreamLogger.d("OnKeyUp, event: " + event);
        boolean handled = false;
        switch(keyCode) {
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                playPause();
                handled = true;
                break;
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
            case KeyEvent.KEYCODE_MEDIA_NEXT:
                videoPlayerWrapper.seekForward();
                handled = true;
                break;
            case KeyEvent.KEYCODE_MEDIA_REWIND:
            case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                videoPlayerWrapper.seekBackward();
                handled = true;
                break;
            case KeyEvent.KEYCODE_MENU:
                onMenuClicked();
                handled = true;
                break;
            case KeyEvent.KEYCODE_BACK:
            case KeyEvent.KEYCODE_ESCAPE:
                handled = dismissBars();
                return handled ? true : super.onKeyUp(keyCode, event);
        }
        showBars();
        return handled ? true : super.onKeyUp(keyCode, event);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if(fromUser) {
            videoPlayerWrapper.seekTo((float) progress / seekBar.getMax());
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isSeeking = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        isSeeking = false;
    }

    @Override
    public void onClick(View v) {
        playPause();
    }

    private void playPause() {
        playPauseButton.setImageResource(videoPlayerWrapper.playerPauseRestart() ? R.drawable.play1 : R
                .drawable.pause1);
    }

    private void updateProgress() {
        if (isSeeking)
            return;
        long[] positionInfo = videoPlayerWrapper.getPositionInfo();
        // Update the progressbar and Time display with current position
        long duration = positionInfo[0];
        long currentPosition = positionInfo[1];
        long minPosition = positionInfo[2];
        long maxPosition = positionInfo[3];

        if (duration < 0) {
            currentTimeText.setVisibility(View.INVISIBLE);
            totalTimeText.setVisibility(View.INVISIBLE);
            int nDuration = (int) (maxPosition - minPosition);
            int nPos = (int) (currentPosition - minPosition);
            seekBar.setProgress(100 * nPos / nDuration);
        } else {
            seekBar.setProgress((int) (100 * currentPosition / duration));
            String str = DateUtils.formatElapsedTime(currentPosition / 1000);
            currentTimeText.setText(str);
            str = DateUtils.formatElapsedTime(duration / 1000);
            totalTimeText.setText(str);
        }
    }


    private void showBars() {
        if(topBar.getVisibility() == View.GONE) {
            topBar.startAnimation(AnimationUtils.loadAnimation(this, R.anim.anim_slide_from_top));
            bottomBar.startAnimation(AnimationUtils.loadAnimation(this, R.anim.anim_slide_from_bottom));
            topBar.setVisibility(View.VISIBLE);
            bottomBar.setVisibility(View.VISIBLE);
            handler.removeCallbacks(dismissBarsRunnable);
            handler.postDelayed(dismissBarsRunnable, DISMISS_BARS_DELAY);
        }
    }

    private boolean dismissBars() {
        if(topBar.getVisibility() == View.VISIBLE) {
            topBar.startAnimation(AnimationUtils.loadAnimation(this, R.anim.anim_slide_to_top));
            bottomBar.startAnimation(AnimationUtils.loadAnimation(this, R.anim.anim_slide_to_bottom));
            topBar.setVisibility(View.GONE);
            bottomBar.setVisibility(View.GONE);
            return true;
        }
        return false;
    }

    private void onMenuClicked() {
        //TODO: use this in order to display a menu for selecting audio, subtitles etc
    }
}