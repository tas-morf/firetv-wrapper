package com.accedo.firetv.player;

import android.util.Log;

/**
 * Handles logging
 */
public class OnStreamLogger {

    private static final String TAG = OnStreamLogger.class.getSimpleName();

    private enum LogLevel {
        VERBOSE,
        DEBUG,
        INFO,
        WARN,
        ERROR
    }
    public static void v(String message) {
        log(message, LogLevel.VERBOSE);
    }

    public static void d(String message) {
        log(message, LogLevel.DEBUG);
    }

    public static void i(String message) {
        log(message, LogLevel.INFO);
    }

    public static void w(String message) {
        log(message, LogLevel.WARN);
    }

    public static void e(String message) {
        log(message, LogLevel.ERROR);
    }

    public static void v(String message, Throwable e) {
        log(message, LogLevel.VERBOSE, e);
    }


    public static void d(String message, Throwable e) {
        log(message, LogLevel.DEBUG);
    }

    public static void i(String message, Throwable e) {
        log(message, LogLevel.INFO);
    }

    public static void w(String message, Throwable e) {
        log(message, LogLevel.WARN);
    }

    public static void e(String message, Throwable e) {
        log(message, LogLevel.ERROR);
    }

    private static void log(String message, LogLevel logLevel) {
        if(!BuildConfig.DEBUG) {
            return;
        }
        switch (logLevel) {
            case VERBOSE:
                Log.v(TAG, message);
                break;
            case DEBUG:
                Log.d(TAG, message);
                break;
            case INFO:
                Log.i(TAG, message);
                break;
            case WARN:
                Log.w(TAG, message);
                break;
            case ERROR:
                Log.e(TAG, message);
                break;
        }
    }

    private static void log(String message, LogLevel logLevel, Throwable e) {
        if(!BuildConfig.DEBUG) {
            return;
        }
        switch (logLevel) {
            case VERBOSE:
                Log.v(TAG, message, e);
                break;
            case DEBUG:
                Log.d(TAG, message, e);
                break;
            case INFO:
                Log.i(TAG, message, e);
                break;
            case WARN:
                Log.w(TAG, message, e);
                break;
            case ERROR:
                Log.e(TAG, message, e);
                break;
        }
    }
}
