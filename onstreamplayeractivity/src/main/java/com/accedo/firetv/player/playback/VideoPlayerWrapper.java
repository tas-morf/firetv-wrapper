package com.accedo.firetv.player.playback;

import android.view.SurfaceView;

/**
 * Plays video content
 */
public interface VideoPlayerWrapper {
    void initialize(String videoPath, String licenseServer, SurfaceView surfaceView, VideoPlayerWrapperListener
            playerListener);

    void surfaceChanged();

    void releaseMediaPlayer();

    boolean playerPauseRestart();

    long[] getPositionInfo();

    void seekForward();

    void seekBackward();

    void seekTo(float durationPercentage);
}
