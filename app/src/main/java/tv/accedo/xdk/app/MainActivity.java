package tv.accedo.xdk.app;

import android.os.Bundle;
import android.webkit.JavascriptInterface;

import com.accedo.firetv.player.OnStreamPlayerActivity;

import tv.accedo.xdk.ext.device.android.shared.BaseActivity;
import tv.accedo.xdk.ext.device.android.shared.XDKWebView;


public class MainActivity extends BaseActivity {

    private static final float WEBSITE_DEFAULT_WIDTH = 1280f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        XDKWebView xdkWebView = (XDKWebView) findViewById(R.id.webview);
        int widthPixels = getResources().getDisplayMetrics().widthPixels;
        xdkWebView.setInitialScale((int) ((float) widthPixels / WEBSITE_DEFAULT_WIDTH * 100));
        //load the html
        //        xdkWebView.loadUrl(getString(R.string.url));
        //        xdkWebView.loadUrl("http://192.168.40.168/webstorm/skystorm-samsung/src/");
//                xdkWebView.loadUrl("http://192.168.40.168/webstorm/maxdome-xdk-next/src/");
        xdkWebView.loadUrl("file:///android_asset/xdksrc/index.html");
        xdkWebView.addJavascriptInterface(this, "AndroidMedia");
        //possible disable cache for development
        //mWebView.clearCache(true);
    }

    @JavascriptInterface
    public void loadVideo(String url, String licenseUrl, String videoTitle) {
        startActivity(OnStreamPlayerActivity.getOnStreamPlayerIntent(this, url, licenseUrl, videoTitle));
    }
}
